<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Code igniter Assignment Project</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row m-4 p-4 justify-content-center">
			<div class="alert alert-danger row ">
				<p>
					If you are visiting this site first time please click on the <strong>Initializen project </strong> button which will create a user table as well as insert demo data.
					Other wise click on the <strong>crud application </strong> button 
				</p>

			</div>

			<div class="col-md-6 text-center">
				<a href="<?php echo base_url('/users-list');?>" class="btn btn-primary">Crud App</a>
				<a href="<?php echo base_url('/Home/install');?>" class="btn btn-danger">Initializen project</a>
			</div>
		</div>
	</div>
</body>
</html>