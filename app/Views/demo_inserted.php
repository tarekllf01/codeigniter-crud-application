<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title> Demo inserted</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div class="container">

        <div class="row m-4 p-4 justify-content-center">
            <div class="alert alert-success col-md-12 ">
                <p>
                    <strong>users table creted</strong><br>
                    <strong> Demo users inserted in users table</strong><br>
                    Now You can go to crud app and test.
                </p>

            </div>
        </div>
        <div class="row m-4 p-4 justify-content-center">
            <div class="col-md-6 text-center">
                <a href="<?php echo base_url('/users-list');?>" class="btn btn-success">Go to Crud App</a>
                <a href="<?php echo base_url('/');?>" class="btn btn-primary">Back to welcome Page</a>
            </div>
        </div>
	</div>
</body>
</html>