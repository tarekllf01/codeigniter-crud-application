<?php

namespace App\Controllers;
use App\Models\UserModel;

class Home extends BaseController
{
	
	public function index()
	{

		return view('welcome_message');
		
	}


	public function install () {
		$forge = \Config\Database::forge();
        $fields =[
            'id'          => [
                    'type'           => 'INT',
                    'constraint'     => 5,
                    'unsigned'       => true,
                    'auto_increment' => true
            ],
            'name'       => [
                    'type'           => 'VARCHAR',
                    'constraint'     => '100',
                    'null'         => true,
            ],
            'email'      => [
                    'type'           =>'VARCHAR',
                    'constraint'     => 200,
                    
            ],
            
        ];

        $forge->addField($fields); 
        $forge->addKey('id',true);
        $forge->createTable('users', TRUE);

        $data = [
            ['name' => 'Tarek Hossen','email' => 'tarekllf01@gmail.com'],
            ['name' => 'MD TAREK','email' => 'tarek@gmail.com'],
            ['name' => 'Luther Hargreeves','email' => 'luther@gmail.com'],
            ['name' => 'Ben Hargreeves','email' => 'ben@gmail.com'],
            
        ];

		$db = \Config\Database::connect();
		foreach ($data as $row) {
			$db->table('users')->insert($row);
		}

		return view('demo_inserted');
        
		

	}
}
