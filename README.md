#steps for hosting in local pc 
#step1: Extract the source code and keep your htdocs folder or server


#step2: open command line in the project directory and enter command composer install

#Step3: Create a database and data base user  from phpMyAdmin  and provide the data base name, database username, database user password in app/Config/Database.php file 

#step4: visit the project  url in browser. 
Example:
url: localhost/crud/public/
url: localhost:8080/crud/public/   (if any port required for your local server enter this after localhost. example localhost:8080)

 
#step5:
Click on Initialize project button to create required table and demo data.
And now you can click crud app and check the CRUD operations of users.

